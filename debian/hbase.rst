=====
HBase
=====

--------------------
The Hadoop Database.
--------------------

:Author: Thomas Koch <thomas.koch@ymc.ch>
:Date: 2010-03-29
:Copyright: 2010 The Apache Software Foundation. All rights reserved.
:Version: 0.1
:Manual section: 1
:Manual group: cluster computing

SYNOPSIS
========

Usage: hbase [--config confdir] COMMAND

DESCRIPTION
===========

HBase is the Hadoop database. It hosts very large tables (think
petabytes) -- billions of rows X millions of columns -- atop clusters of
commodity hardware. It's modeled after Google's Bigtable.

OPTIONS
=======

--config configdir
           Overrides the "HADOOP_CONF_DIR" environment variable.  See "ENVI‐
           RONMENT" section below.

COMMANDS
========


FILES
=====

/etc/hbase/conf
    This symbolic link points to the currently active HBase configuration directory.

Note to System Admins

The "/etc/hbase/conf" link is managed by the alternatives(8) command so you
should not change this symlink directly.

ENVIRONMENT
===========


EXAMPLES
========


BUGS
====


SEE ALSO
========

alternatives(8)
